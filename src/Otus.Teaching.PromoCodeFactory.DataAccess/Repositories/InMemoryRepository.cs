﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T : BaseEntity
    {
        protected List<T> Data { get; set; }

        public InMemoryRepository(List<T> data)
        {
            Data = data;
        }

        public Task<List<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task AddAsync(T item)
        {
            Data.Add(item);
            return Task.CompletedTask;
        }
        public Task UpdateAsync(Employee employee)
        {
            if (Data is List<Employee> eData)
            {
                var claimEmployee = eData.FirstOrDefault(x => x.Id == employee.Id);
                claimEmployee = employee;
            }
            return Task.CompletedTask;

        }

        public Task DeleteByIdAsync(Guid id)
        {
            Data.Remove(Data.FirstOrDefault(x => x.Id == id));
            return Task.CompletedTask;
        }

    }
}